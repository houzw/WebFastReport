library WebPrinterActiveX;

uses
  ComServ,
  WebPrinterActiveX_TLB in 'WebPrinterActiveX_TLB.pas',
  WebPrinterImpl in 'WebPrinterImpl.pas' {WebPrinter: TActiveForm} {WebPrinter: CoClass},
  WebFastReport in '..\FastReport\WebFastReport.pas',
  SysComm in '..\FastReport\SysComm.pas',
  ReportDataLoader in '..\FastReport\ReportDataLoader.pas',
  ReportData in '..\FastReport\ReportData.pas',
  NativeXml in '..\FastReport\NativeXml.pas',
  ICS_WSocket in '..\FastReport\ICS_WSocket.pas',
  ICS_WSockBuf in '..\FastReport\ICS_WSockBuf.pas',
  ICS_HttpProt in '..\FastReport\ICS_HttpProt.pas';

{$E ocx}

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R *.TLB}

{$R *.RES}

begin
end.
