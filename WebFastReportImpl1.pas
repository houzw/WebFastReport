unit WebFastReportImpl1;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ActiveX, AxCtrls, WebFastReportProj_TLB, StdVcl, StdCtrls, Buttons,
  frxClass, frxDesgn, DB, Grids, DBGrids,ReportData,DBClient,frxBarcode,NativeXml,frxDBSet,
  frxExportXML;
type
  TWebFastReportFrm = class(TActiveForm, IWebFastReport)
    Label1: TLabel;
    Label2: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    Button1: TButton;
    Button2: TButton;
    Button4: TButton;
    OpenDialog1: TOpenDialog;
    OpenDialog2: TOpenDialog;
    frxDesigner1: TfrxDesigner;
    frxReport1: TfrxReport;
    frxXMLExport1: TfrxXMLExport;
    procedure initReport(designFile:WideString;dataprovide:WideString);
    procedure PrintReport();
    procedure ShowReport();
    procedure DesignReport();
    function frxDesigner1SaveReport(Report: TfrxReport;SaveAs: Boolean): Boolean;
    procedure frxReport1GetValue(const VarName: String;var Value: Variant);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Edit1DblClick(Sender: TObject);
    procedure Edit2DblClick(Sender: TObject);
    procedure Button4Click(Sender: TObject);
  private
    { Private declarations }
    //参数变量
    App:WideString;

    FEvents: IWebFastReportEvents;
    procedure ActivateEvent(Sender: TObject);
    procedure ClickEvent(Sender: TObject);
    procedure CreateEvent(Sender: TObject);
    procedure DblClickEvent(Sender: TObject);
    procedure DeactivateEvent(Sender: TObject);
    procedure DestroyEvent(Sender: TObject);
    procedure KeyPressEvent(Sender: TObject; var Key: Char);
    procedure PaintEvent(Sender: TObject);
  protected
    { Protected declarations }
    procedure DefinePropertyPages(DefinePropertyPage: TDefinePropertyPage); override;
    procedure EventSinkChanged(const EventSink: IUnknown); override;
    function Get_Active: WordBool; safecall;
    function Get_AlignDisabled: WordBool; safecall;
    function Get_AutoScroll: WordBool; safecall;
    function Get_AutoSize: WordBool; safecall;
    function Get_AxBorderStyle: TxActiveFormBorderStyle; safecall;
    function Get_Caption: WideString; safecall;
    function Get_Color: OLE_COLOR; safecall;
    function Get_DoubleBuffered: WordBool; safecall;
    function Get_DropTarget: WordBool; safecall;
    function Get_Enabled: WordBool; safecall;
    function Get_Font: IFontDisp; safecall;
    function Get_HelpFile: WideString; safecall;
    function Get_KeyPreview: WordBool; safecall;
    function Get_PixelsPerInch: Integer; safecall;
    function Get_PrintScale: TxPrintScale; safecall;
    function Get_Scaled: WordBool; safecall;
    function Get_ScreenSnap: WordBool; safecall;
    function Get_SnapBuffer: Integer; safecall;
    function Get_Visible: WordBool; safecall;
    function Get_VisibleDockClientCount: Integer; safecall;
    procedure _Set_Font(var Value: IFontDisp); safecall;
    procedure Set_AutoScroll(Value: WordBool); safecall;
    procedure Set_AutoSize(Value: WordBool); safecall;
    procedure Set_AxBorderStyle(Value: TxActiveFormBorderStyle); safecall;
    procedure Set_Caption(const Value: WideString); safecall;
    procedure Set_Color(Value: OLE_COLOR); safecall;
    procedure Set_DoubleBuffered(Value: WordBool); safecall;
    procedure Set_DropTarget(Value: WordBool); safecall;
    procedure Set_Enabled(Value: WordBool); safecall;
    procedure Set_Font(const Value: IFontDisp); safecall;
    procedure Set_HelpFile(const Value: WideString); safecall;
    procedure Set_KeyPreview(Value: WordBool); safecall;
    procedure Set_PixelsPerInch(Value: Integer); safecall;
    procedure Set_PrintScale(Value: TxPrintScale); safecall;
    procedure Set_Scaled(Value: WordBool); safecall;
    procedure Set_ScreenSnap(Value: WordBool); safecall;
    procedure Set_SnapBuffer(Value: Integer); safecall;
    procedure Set_Visible(Value: WordBool); safecall;
    procedure Print(mode: SYSINT; const designFile, dataprovide: WideString);safecall;
    function Get_App: WideString; safecall;
    procedure Set_App(const Value: WideString); safecall;
  public
    { Public declarations }
    AReportData:TReportData;
    procedure Initialize; override;
  end;

implementation

uses ComObj, ComServ;

{$R *.DFM}

{ TWebFastReport }

procedure TWebFastReportFrm.DefinePropertyPages(DefinePropertyPage: TDefinePropertyPage);
begin
  { Define property pages here.  Property pages are defined by calling
    DefinePropertyPage with the class id of the page.  For example,
      DefinePropertyPage(Class_WebFastReportPage); }
end;

procedure TWebFastReportFrm.EventSinkChanged(const EventSink: IUnknown);
begin
  FEvents := EventSink as IWebFastReportEvents;
  inherited EventSinkChanged(EventSink);
end;

procedure TWebFastReportFrm.Initialize;
begin
  inherited Initialize;
  OnActivate := ActivateEvent;
  OnClick := ClickEvent;
  OnCreate := CreateEvent;
  OnDblClick := DblClickEvent;
  OnDeactivate := DeactivateEvent;
  OnDestroy := DestroyEvent;
  OnKeyPress := KeyPressEvent;
  OnPaint := PaintEvent;
end;

function TWebFastReportFrm.Get_Active: WordBool;
begin
  Result := Active;
end;

function TWebFastReportFrm.Get_AlignDisabled: WordBool;
begin
  Result := AlignDisabled;
end;

function TWebFastReportFrm.Get_AutoScroll: WordBool;
begin
  Result := AutoScroll;
end;

function TWebFastReportFrm.Get_AutoSize: WordBool;
begin
  Result := AutoSize;
end;

function TWebFastReportFrm.Get_AxBorderStyle: TxActiveFormBorderStyle;
begin
  Result := Ord(AxBorderStyle);
end;

function TWebFastReportFrm.Get_Caption: WideString;
begin
  Result := WideString(Caption);
end;

function TWebFastReportFrm.Get_Color: OLE_COLOR;
begin
  Result := OLE_COLOR(Color);
end;

function TWebFastReportFrm.Get_DoubleBuffered: WordBool;
begin
  Result := DoubleBuffered;
end;

function TWebFastReportFrm.Get_DropTarget: WordBool;
begin
  Result := DropTarget;
end;

function TWebFastReportFrm.Get_Enabled: WordBool;
begin
  Result := Enabled;
end;

function TWebFastReportFrm.Get_Font: IFontDisp;
begin
  GetOleFont(Font, Result);
end;

function TWebFastReportFrm.Get_HelpFile: WideString;
begin
  Result := WideString(HelpFile);
end;

function TWebFastReportFrm.Get_KeyPreview: WordBool;
begin
  Result := KeyPreview;
end;

function TWebFastReportFrm.Get_PixelsPerInch: Integer;
begin
  Result := PixelsPerInch;
end;

function TWebFastReportFrm.Get_PrintScale: TxPrintScale;
begin
  Result := Ord(PrintScale);
end;

function TWebFastReportFrm.Get_Scaled: WordBool;
begin
  Result := Scaled;
end;

function TWebFastReportFrm.Get_ScreenSnap: WordBool;
begin
  Result := ScreenSnap;
end;

function TWebFastReportFrm.Get_SnapBuffer: Integer;
begin
  Result := SnapBuffer;
end;

function TWebFastReportFrm.Get_Visible: WordBool;
begin
  Result := Visible;
end;

function TWebFastReportFrm.Get_VisibleDockClientCount: Integer;
begin
  Result := VisibleDockClientCount;
end;

procedure TWebFastReportFrm._Set_Font(var Value: IFontDisp);
begin
  SetOleFont(Font, Value);
end;

procedure TWebFastReportFrm.ActivateEvent(Sender: TObject);
begin
  if FEvents <> nil then FEvents.OnActivate;
end;

procedure TWebFastReportFrm.ClickEvent(Sender: TObject);
begin
  if FEvents <> nil then FEvents.OnClick;
end;

procedure TWebFastReportFrm.CreateEvent(Sender: TObject);
begin
  if FEvents <> nil then FEvents.OnCreate;
end;

procedure TWebFastReportFrm.DblClickEvent(Sender: TObject);
begin
  if FEvents <> nil then FEvents.OnDblClick;
end;

procedure TWebFastReportFrm.DeactivateEvent(Sender: TObject);
begin
  if FEvents <> nil then FEvents.OnDeactivate;
end;

procedure TWebFastReportFrm.DestroyEvent(Sender: TObject);
begin
  if FEvents <> nil then FEvents.OnDestroy;
end;

procedure TWebFastReportFrm.KeyPressEvent(Sender: TObject; var Key: Char);
var
  TempKey: Smallint;
begin
  TempKey := Smallint(Key);
  if FEvents <> nil then FEvents.OnKeyPress(TempKey);
  Key := Char(TempKey);
end;

procedure TWebFastReportFrm.PaintEvent(Sender: TObject);
begin
  if FEvents <> nil then FEvents.OnPaint;
end;

procedure TWebFastReportFrm.Set_AutoScroll(Value: WordBool);
begin
  AutoScroll := Value;
end;

procedure TWebFastReportFrm.Set_AutoSize(Value: WordBool);
begin
  AutoSize := Value;
end;

procedure TWebFastReportFrm.Set_AxBorderStyle(Value: TxActiveFormBorderStyle);
begin
  AxBorderStyle := TActiveFormBorderStyle(Value);
end;

procedure TWebFastReportFrm.Set_Caption(const Value: WideString);
begin
  Caption := TCaption(Value);
end;

procedure TWebFastReportFrm.Set_Color(Value: OLE_COLOR);
begin
  Color := TColor(Value);
end;

procedure TWebFastReportFrm.Set_DoubleBuffered(Value: WordBool);
begin
  DoubleBuffered := Value;
end;

procedure TWebFastReportFrm.Set_DropTarget(Value: WordBool);
begin
  DropTarget := Value;
end;

procedure TWebFastReportFrm.Set_Enabled(Value: WordBool);
begin
  Enabled := Value;
end;

procedure TWebFastReportFrm.Set_Font(const Value: IFontDisp);
begin
  SetOleFont(Font, Value);
end;

procedure TWebFastReportFrm.Set_HelpFile(const Value: WideString);
begin
  HelpFile := String(Value);
end;

procedure TWebFastReportFrm.Set_KeyPreview(Value: WordBool);
begin
  KeyPreview := Value;
end;

procedure TWebFastReportFrm.Set_PixelsPerInch(Value: Integer);
begin
  PixelsPerInch := Value;
end;

procedure TWebFastReportFrm.Set_PrintScale(Value: TxPrintScale);
begin
  PrintScale := TPrintScale(Value);
end;

procedure TWebFastReportFrm.Set_Scaled(Value: WordBool);
begin
  Scaled := Value;
end;

procedure TWebFastReportFrm.Set_ScreenSnap(Value: WordBool);
begin
  ScreenSnap := Value;
end;

procedure TWebFastReportFrm.Set_SnapBuffer(Value: Integer);
begin
  SnapBuffer := Value;
end;

procedure TWebFastReportFrm.Set_Visible(Value: WordBool);
begin
  Visible := Value;
end;
procedure TWebFastReportFrm.Button1Click(Sender: TObject);
var
  Report:TWebFastReport;
begin
  self.initReport('','');
  Self.frxReport1.DesignReport;
end;

procedure TWebFastReportFrm.Button2Click(Sender: TObject);
begin
  Self.initReport('','');
  //Self.AReport.ShowReport;
  self.frxReport1.ShowReport;
end;

function TWebFastReportFrm.frxDesigner1SaveReport(Report: TfrxReport;
  SaveAs: Boolean): Boolean;
var
  saveDialog:TSaveDialog;
  i:Integer;
  masterDataName:string;
  AComponent:TfrxComponent;
  AMasterData:TFrxMasterData;
begin
  AComponent:=nil;
  AMasterData:=nil;
  if SaveAs then
  begin
    saveDialog:= TSaveDialog.Create(self);
    saveDialog.Filter:='报表设计文件(*.fr3)|*.fr3';
    if saveDialog.Execute then
    begin
       //清除变量

       for i := 0 to Report.Variables.Count-1 do
       begin
          Report.Variables.DeleteVariable(Report.Variables.items[0].Name);    //使用items[0],因为删除时，变量个数在变化
       end;

       //清除数据集
       for  i:=0  to Report.DataSets.Count-1 do
       begin
          masterDataName:='MasterData_'+Report.DataSets.Items[i].DataSetName;
          AComponent:=Report.FindObject(masterDataName);
          if Assigned(AComponent) and (AComponent is TfrxMasterData) then
          begin
              TfrxMasterData(AComponent).DataSet:=nil;
          end;
       end;
       Report.DataSets.Clear;
       Report.SaveToFile(saveDialog.FileName);
       //提示保存的状态并告知路径
    end;
  end;
  result:=true;
end;

procedure TWebFastReportFrm.frxReport1GetValue(const VarName: String;
  var Value: Variant);
var
  i:Integer;
  ARVariable: RVariable;
begin
  for i := 0 to AReportData.getVariableCount -1  do
  begin
    ARVariable:=AReportData.getRVariable(i);
    if VarName = ARVariable.key then Value:= ARVariable.value;
  end;
end;

procedure TWebFastReportFrm.Edit1DblClick(Sender: TObject);
var
  name,path:string;
begin
  Self.OpenDialog1.Filter:='报表设计文件(*.fr3)|*.fr3';
  if self.OpenDialog1.Execute then
  begin
    name := extractfilename(opendialog1.FileName);
    path := extractfiledir(opendialog1.FileName);
    self.Edit1.Text:=path+'\'+name;
  end;
end;

procedure TWebFastReportFrm.Edit2DblClick(Sender: TObject);
var
  name,path:string;
begin
  Self.OpenDialog2.Filter:='报表数据文件(*.xml)|*.xml';
  if self.OpenDialog2.Execute then
  begin
    name := extractfilename(opendialog2.FileName);
    path := extractfiledir(opendialog2.FileName);
    self.Edit2.Text:=path+'\'+name;
  end;
end;

procedure TWebFastReportFrm.Button4Click(Sender: TObject);
begin
  self.Print(3,'','');
end;
procedure TWebFastReportFrm.DesignReport;
begin
  Self.initReport('','');
  self.frxReport1.DesignReport;
end;

procedure TWebFastReportFrm.PrintReport;
begin
  Self.initReport('','');
  Self.frxReport1.PrepareReport;
  Self.frxReport1.PrintOptions.ShowDialog:=false;
  if Self.frxReport1.Print then
  begin
    ShowMessage('打印完成！');
  end;
end;

procedure TWebFastReportFrm.ShowReport;
begin
  Self.initReport('','');
  Self.frxReport1.ShowReport;
end;

procedure TWebFastReportFrm.Print(mode: SYSINT; const designFile,dataprovide: WideString);
begin
  if mode=1 then
  begin
    Self.DesignReport;
  end
  else if mode = 2 then
  begin
    Self.ShowReport;
  end
  else
  begin
    self.PrintReport;
  end;
end;

function TWebFastReportFrm.Get_App: WideString;
begin
   Result:=Self.App;
end;

procedure TWebFastReportFrm.Set_App(const Value: WideString);
begin
   self.App:=Value;
end;

procedure TWebFastReportFrm.initReport(designFile, dataprovide: WideString);
var
  i,j:Integer;
  ADataSetName:string;
  AFrxDBDataSet:TFrxDBDataSet;
  AMasterData:TFrxMasterData;
  AStream:TFileStream;
  AClientDataSet:TClientDataSet;
  ARDataSet:RDataSet;
  ARVariable:RVariable;
  flag:Boolean;
begin
  flag:=False;
  AReportData:= TReportData.Create(Self);
  AStream:= TFileStream.Create(Self.Edit2.Text,SysUtils.fmOpenRead);
  if Assigned(AStream) then
  begin
    AReportData.load(AStream);
  end;
  //if Assigned(AReport) then
  if Assigned(Self.frxReport1) then
  begin
      //FreeAndNil(AReport);
      FreeAndNil(self.frxreport1);
  end;
//    AReport:=TFrxReport.Create(self);
    Self.frxReport1:=TfrxReport.Create(self);
    Self.frxReport1.OnGetValue := Self.frxReport1GetValue;
//  AReport.LoadFromFile(self.Edit1.Text);
    Self.frxReport1.LoadFromFile(self.Edit1.Text);
  //报表变量加载
  for i := 0 to AReportData.getVariableCount -1  do
  begin
    if i=0 then Self.frxReport1.Variables.Add.Name:= ' def';
    ARVariable:=AReportData.getRVariable(i);
    Self.frxReport1.Variables.AddVariable('def',ARVariable.key,ARVariable.value);
  end;

  //报表数据集加载
  for  i:=0  to AReportData.getDataSetCount-1 do
  begin
    ARDataSet:=AReportData.getRDataSet(i);
    ADataSetName:=ARDataSet.name;
    AClientDataSet:=ARDataSet.dataset;

    for j := 0 to self.ComponentCount-1 do
    begin
       if (Self.Components[j] is  TFrxDBDataset) and  (TFrxDBDataset(Self.Components[j]).Name = ADataSetName) then
       begin
          flag:=True;
          AFrxDBDataSet:= TFrxDBDataset(Self.Components[j]);
       end;
    end;

    if not flag then
    begin
      AFrxDBDataSet:=TFrxDBDataset.Create(self);
      AFrxDBDataSet.Name:=ADataSetName;
      AFrxDBDataSet.UserName:=ADataSetName;
    end;
    flag:=False;
    AFrxDBDataSet.DataSet:=AClientDataSet;
    Self.frxReport1.DataSets.Add(AFrxDBDataSet);
    
    AMasterData:= TFrxMasterData(Self.frxReport1.FindObject('MasterData_'+ADataSetName));
    if Assigned(AMasterData) then
    begin
      AMasterData.DataSet:=AFrxDBDataSet;
    end;
  end;
  AStream.Free;
  //AReportData.destroy;
  //报表准备完成，等待预览、设计或打印
end;

initialization
  TActiveFormFactory.Create(
    ComServer,
    TActiveFormControl,
    TWebFastReportFrm,
    CLASS_WebFastReportFrm,
    1,
    '',
    OLEMISC_SIMPLEFRAME or OLEMISC_ACTSLIKELABEL,
    tmApartment);
end.
