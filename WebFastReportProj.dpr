library WebFastReportProj;

uses
  ComServ,
  WebFastReportProj_TLB in 'WebFastReportProj_TLB.pas',
  WebFastReportImpl1 in 'WebFastReportImpl1.pas' {WebFastReportFrm: TActiveForm} {WebFastReport: CoClass},
  NativeXml in 'NativeXml.pas',
  ReportData in 'ReportData.pas',
  ReportDataLoader in 'ReportDataLoader.pas',
  WebFastReport in 'WebFastReport.pas',
  ICS_HttpProt in 'ICS_HttpProt.pas',
  ICS_WSockBuf in 'ICS_WSockBuf.pas',
  ICS_WSocket in 'ICS_WSocket.pas',
  SysComm in 'SysComm.pas';

{$E ocx}

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R *.TLB}

{$R *.RES}

begin
end.
