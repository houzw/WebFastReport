unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls,NativeXml,ReportData, Grids, DBGrids, DB,SysComm,PerlRegEx,
  frxClass, frxDesgn, frxBarcode,frxDBSet,DBClient,ReportDataLoader,WebFastReport,
  frxExportODF, frxExportTXT, frxExportMail, frxExportCSV, frxExportText,
  frxExportImage, frxExportRTF, frxExportXML, frxExportXLS, frxExportHTML,
  frxExportPDF;
type
  TForm1 = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    OpenDialog1: TOpenDialog;
    OpenDialog2: TOpenDialog;
    Edit1: TEdit;
    Edit2: TEdit;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    DBGrid1: TDBGrid;
    DataSource1: TDataSource;
    DBGrid2: TDBGrid;
    DataSource2: TDataSource;
    Button4: TButton;
    frxReport1: TfrxReport;
    Button5: TButton;
    Button6: TButton;
    Memo1: TMemo;
    Memo2: TMemo;
    Button7: TButton;
    Button8: TButton;
    Button9: TButton;
    frxPDFExport1: TfrxPDFExport;
    frxXLSExport1: TfrxXLSExport;
    frxRTFExport1: TfrxRTFExport;
    frxCSVExport1: TfrxCSVExport;
    procedure Edit1DblClick(Sender: TObject);
    procedure Edit2DblClick(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    function frxDesigner1SaveReport(Report: TfrxReport;SaveAs: Boolean): Boolean;
    procedure Button2Click(Sender: TObject);
    procedure initReport();
    procedure Button1Click(Sender: TObject);
    procedure frxReport1GetValue(const VarName: String;var Value: Variant);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure Button9Click(Sender: TObject);
  private
    { Private declarations }
  public
    //AReport:TfrxReport;
    AReportData:TReportData;
    { Public declarations }
  end;
var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Edit1DblClick(Sender: TObject);
var
  name,path:string;
begin
  Self.OpenDialog1.Filter:='报表设计文件(*.fr3)|*.fr3';
  if self.OpenDialog1.Execute then
  begin
    name := extractfilename(opendialog1.FileName);
    path := extractfiledir(opendialog1.FileName);
    self.Edit1.Text:=path+'\'+name;
  end;
end;

procedure TForm1.Edit2DblClick(Sender: TObject);
var
  name,path:string;
begin
  Self.OpenDialog2.Filter:='报表数据文件(*.xml)|*.xml';
  if self.OpenDialog2.Execute then
  begin
    name := extractfilename(opendialog2.FileName);
    path := extractfiledir(opendialog2.FileName);
    self.Edit2.Text:=path+'\'+name;
  end;
end;
procedure TForm1.Button3Click(Sender: TObject);
var
  ReportData:TReportData;
  stream:TFileStream;
begin
  ReportData:= TReportData.Create(Self);
  stream:= TFileStream.Create(Self.Edit2.Text,SysUtils.fmOpenRead);
  ReportData.load(stream);
  Application.MessageBox(PAnsiChar(ReportData.getVariable('title')),'提示title');
  Application.MessageBox(PAnsiChar(ReportData.getVariable('name')),'提示name');
  Self.DataSource1.DataSet:=ReportData.getDataSet('ds1');
  Self.DataSource2.DataSet:=ReportData.getDataSet('ds2');
  Self.DBGrid1.DataSource:=Self.DataSource1;
  Self.DBGrid2.DataSource:=self.DataSource2;
  ReportData.destroy;
  stream.Free;
end;

function TForm1.frxDesigner1SaveReport(Report: TfrxReport;
  SaveAs: Boolean): Boolean;
var
  saveDialog:TSaveDialog;
  i:Integer;
  masterDataName:string;
  AComponent:TfrxComponent;
  AMasterData:TFrxMasterData;
  p:WideString;
begin
  AComponent:=nil;
  AMasterData:=nil;
  if true then
  begin
    //saveDialog:= TSaveDialog.Create(self);
    //saveDialog.Filter:='报表设计文件(*.fr3)|*.fr3';
    //if saveDialog.Execute then
    //begin
       //清除变量

       for i := 0 to Report.Variables.Count-1 do
       begin
          Report.Variables.DeleteVariable(Report.Variables.items[0].Name);    //使用items[0],因为删除时，变量个数在变化
       end;

       //清除数据集
       for  i:=0  to Report.DataSets.Count-1 do
       begin
          masterDataName:='MasterData_'+Report.DataSets.Items[i].DataSetName;
          AComponent:=Report.FindObject(masterDataName);
          if Assigned(AComponent) and (AComponent is TfrxMasterData) then
          begin
              TfrxMasterData(AComponent).DataSet:=nil;
          end;
       end;
       Report.DataSets.Clear;
       p:=GetEnvironmentVariable('USERPROFILE')+'\WebFastReport\ship';
       if not DirectoryExists(p) then
          ForceDirectories(p);
       Report.SaveToFile(p+'\test.fr3');
    //end;
  end;
  result:=true;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  Self.initReport;
  //Self.AReport.ShowReport;
  self.frxReport1.ShowReport; 
end;

procedure TForm1.initReport;
var
  i,j:Integer;
  ADataSetName:string;
  AFrxDBDataSet:TFrxDBDataSet;
  AMasterData:TFrxMasterData;
  //AStream:TFileStream;
  ADataStream:TStream;
  ADesignStream:TStream;
  AReportDataLoader:TReportDataLoader;
  AClientDataSet:TClientDataSet;
  ARDataSet:RDataSet;
  ARVariable:RVariable;
  flag:Boolean;
begin
  flag:=False;
  AReportData:= TReportData.Create(Self);
  //AStream:= TFileStream.Create(Self.Edit2.Text,SysUtils.fmOpenRead);
  try
  AReportDataLoader:=TReportDataLoader.create(Self,'ship','test.fr3','http://localhost:8080/FileStore/report.shtml');
  except
    ShowMessage('000000');
  end;
  //AStream:=TStringStream.Create(AReportDataLoader.GetResponseText);
  ADataStream:=AReportDataLoader.GetDataStream;
  ADesignStream:=AReportDataLoader.GetDesignStream;
  if Assigned(ADataStream) then
  begin
    AReportData.load(ADataStream);
  end;
  //if Assigned(AReport) then
  if Assigned(Self.frxReport1) then
  begin
      //FreeAndNil(AReport);
      FreeAndNil(self.frxreport1);
  end;
//    AReport:=TFrxReport.Create(self);
  Self.frxReport1:=TfrxReport.Create(self);
  Self.frxReport1.OnGetValue := Self.frxReport1GetValue;
//  AReport.LoadFromFile(self.Edit1.Text);
//    Self.frxReport1.LoadFromFile(self.Edit1.Text);
  Self.frxReport1.LoadFromStream(ADesignStream);
  //报表变量加载
  for i := 0 to AReportData.getVariableCount -1  do
  begin
    if i=0 then Self.frxReport1.Variables.Add.Name:= ' def';
    ARVariable:=AReportData.getRVariable(i);
    Self.frxReport1.Variables.AddVariable('def',ARVariable.key,ARVariable.value);
  end;

  //报表数据集加载
  for  i:=0  to AReportData.getDataSetCount-1 do
  begin
    ARDataSet:=AReportData.getRDataSet(i);
    ADataSetName:=ARDataSet.name;
    AClientDataSet:=ARDataSet.dataset;

    for j := 0 to self.ComponentCount-1 do
    begin
       if (Self.Components[j] is  TFrxDBDataset) and  (TFrxDBDataset(Self.Components[j]).Name = ADataSetName) then
       begin
          flag:=True;
          AFrxDBDataSet:= TFrxDBDataset(Self.Components[j]);
       end;
    end;

    if not flag then
    begin
      AFrxDBDataSet:=TFrxDBDataset.Create(self);
      AFrxDBDataSet.Name:=ADataSetName;
      AFrxDBDataSet.UserName:=ADataSetName;
    end;
    flag:=False;
    AFrxDBDataSet.DataSet:=AClientDataSet;
    Self.frxReport1.DataSets.Add(AFrxDBDataSet);
    
    AMasterData:= TFrxMasterData(Self.frxReport1.FindObject('MasterData_'+ADataSetName));
    if Assigned(AMasterData) then
    begin
      AMasterData.DataSet:=AFrxDBDataSet;
    end;
  end;
  ADataStream.Free;
  ADesignStream.Free;
  //FreeAndNil(AReportDataLoader);
  AReportDataLoader.Free;
  //AReportDataLoader.destroy;
  AReportData.destroy;
  //报表准备完成，等待预览、设计或打印
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  self.initReport;
  Self.frxReport1.DesignReport;
end;
procedure TForm1.frxReport1GetValue(const VarName: String;
  var Value: Variant);
var
  i:Integer;
  ARVariable: RVariable;
begin
  for i := 0 to AReportData.getVariableCount -1  do
  begin
    ARVariable:=AReportData.getRVariable(i);
    if VarName = ARVariable.key then Value:= ARVariable.value;
  end;
end;

procedure TForm1.Button5Click(Sender: TObject);
begin
ShowMessage(GetEnvironmentVariable('USERPROFILE'));
end;

procedure TForm1.Button6Click(Sender: TObject);
var
  AReportDataLoader:TReportDataLoader;
begin
  AReportDataLoader:=TReportDataLoader.create(Self,'dd','1111','','http://localhost:8080/FileStore/report.shtml');
  Self.Memo1.Text:=AReportDataLoader.GetDataText;
  Self.Memo2.Text:=AReportDataLoader.GetDesignText;
  AReportDataLoader.Free;
end;
procedure TForm1.Button7Click(Sender: TObject);
var
  Report:TWebFastReport;
begin
  Report:=TWebFastReport.create(Self,'ship','test','a=中国&b=zhangao','http://localhost:8080/FileStore/report.shtml?c=zhangaoxiang&d=aaaaa法国');
  Report.showReport;
  FreeAndNil(Report);
end;

procedure TForm1.Button8Click(Sender: TObject);
var
  Report:TWebFastReport;
begin
  Report:=TWebFastReport.create(Self,'ship','test','','http://localhost:8080/FileStore/report.shtml');
  Report.printReport;
  FreeAndNil(Report);
end;
procedure TForm1.Button9Click(Sender: TObject);
var
  Report:TWebFastReport;
begin
  Report:=TWebFastReport.create(Self,'ship','test','','http://localhost:8080/FileStore/report.shtml');
  Report.designReport;
  FreeAndNil(Report);
end;

end.
