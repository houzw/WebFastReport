program FastReport;

uses
  Forms,
  Unit1 in 'Unit1.pas' {Form1},
  NativeXml in 'NativeXml.pas',
  ReportData in 'ReportData.pas',
  ReportDataLoader in 'ReportDataLoader.pas',
  ICS_HttpProt in 'ICS_HttpProt.pas',
  ICS_WSockBuf in 'ICS_WSockBuf.pas',
  ICS_WSocket in 'ICS_WSocket.pas',
  SysComm in 'SysComm.pas',
  WebFastReport in 'WebFastReport.pas',
  Unit3 in 'FastReport\Unit3.pas' {Form3};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TForm3, Form3);
  Application.Run;
end.
