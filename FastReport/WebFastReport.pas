unit WebFastReport;

interface
uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls,Forms,
  Dialogs, StdCtrls,NativeXml,ReportData, Grids, DBGrids, DB,SysComm,PerlRegEx,
  frxClass, frxDesgn, frxBarcode,frxDBSet,DBClient,ReportDataLoader,
  IdCoderMIME,IdGlobal,jpeg;
type
  TWebFastReport = class
  private
     Owner:TComponent;

     URL:string;
     AppName:string;
     ReportName:string;
     Params:string;

     DataSetName:string;
     DataSet:RDataSet;
     Variable:RVariable;
     
     ReportData:TReportData;
     ReportDataLoader:TReportDataLoader;
     DataStream:TStream;
     DesignStream:TStream;
     FrxReport:TfrxReport;
     FrxDBDataSet:TFrxDBDataSet;
     MasterData:TFrxMasterData;
     ClientDataSet:TClientDataSet;
     FrxDesgn:TfrxDesigner;

     procedure init(IsCreate:Boolean=False);
     procedure OnGetValue(const VarName: String;var Value: Variant);
  public
     constructor create(Aowner:TComponent;AappName:string;AreportName:string;AParams:string;AUrl:string);
     destructor destroy(); override;
     function OnSaveReport(Report: TfrxReport;SaveAs: Boolean): Boolean;
     procedure showReport();
     procedure printReport();
     procedure designReport();
     procedure createReport();
     
  end;
implementation

{ TWebFastReport }

constructor TWebFastReport.create(Aowner:TComponent;AappName:string; AreportName: string;AParams:string;AUrl: string);
begin
  Self.Owner:=Aowner;
  Self.AppName:=AappName;
  Self.ReportName:=AreportName;
  self.URL:=AUrl;
  Self.Params:=AParams;
end;
procedure TWebFastReport.showReport;
begin
  Self.init();
  Self.FrxReport.ShowReport;
  FreeAndNil(Self.FrxReport);
end;
procedure TWebFastReport.printReport;
begin
  Self.init();
  Self.FrxReport.PrepareReport();
  Self.FrxReport.PrintOptions.ShowDialog:=false;
  if self.FrxReport.Print then
  begin
    ShowMessage('打印完成！');
  end;
  FreeAndNil(Self.FrxReport);
end;
procedure TWebFastReport.designReport;
begin
  Self.init();
  Self.FrxReport.DesignReport;
  FreeAndNil(Self.FrxReport);
end;
procedure TWebFastReport.init(IsCreate:boolean);
var
  i,j:Integer;
  flag:Boolean;
begin
  flag:=False;
  Self.ReportData:= TReportData.Create(Self.Owner);
  try
    Self.ReportDataLoader:=TReportDataLoader.create(Self.Owner,Self.AppName,Self.ReportName,Self.URL,Self.Params,IsCreate);
  except
    ShowMessage('加载报表数据失败!请检查网络或联系系统管理员...');
  end;
  Self.DataStream:=Self.ReportDataLoader.GetDataStream;
  if not IsCreate then
  begin
    Self.DesignStream:=Self.ReportDataLoader.GetDesignStream;
  end;
  if Assigned(Self.DataStream) then
  begin
    Self.ReportData.load(Self.DataStream);
  end;
  if Assigned(Self.FrxReport) then
  begin
      FreeAndNil(self.FrxReport);
  end;
  Self.FrxDesgn:=TfrxDesigner.Create(Self.Owner);
  Self.FrxReport:=TfrxReport.Create(self.Owner);
  Self.FrxReport.PreviewOptions.Buttons:=[pbPrint, pbExport, pbZoom, pbFind,pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick];
  Self.FrxDesgn.OnSaveReport:=Self.OnSaveReport;
  Self.FrxReport.OnGetValue := Self.OnGetValue;
  if IsCreate then
  begin
    Self.FrxReport.LoadFromFile(ReportDataLoader.GetClientSavePath(self.AppName,self.ReportName));
  end
  else
  begin
    Self.FrxReport.LoadFromStream(Self.DesignStream);
  end;
  //报表变量加载
  for i := 0 to Self.ReportData.getVariableCount -1  do
  begin
    if i=0 then Self.FrxReport.Variables.Add.Name:= ' def';
    Self.Variable:=Self.ReportData.getRVariable(i);
    Self.FrxReport.Variables.AddVariable('def',Self.Variable.key,Self.Variable.value);
  end;
  //报表数据集加载
  for  i:=0  to Self.ReportData.getDataSetCount-1 do
  begin
    Self.DataSet:=Self.ReportData.getRDataSet(i);
    Self.DataSetName:=Self.DataSet.name;
    Self.ClientDataSet:=Self.DataSet.dataset;
    for j := 0 to self.Owner.ComponentCount-1 do
    begin
       if (Self.Owner.Components[j] is  TFrxDBDataset) and  (TFrxDBDataset(Self.Owner.Components[j]).Name = Self.DataSetName) then
       begin
          flag:=True;
          FrxDBDataSet := TFrxDBDataset(Self.Owner.Components[j]);
       end;
    end;

    if not flag then
    begin
      Self.FrxDBDataSet:=TFrxDBDataset.Create(self.Owner);
      Self.FrxDBDataSet.Name:=Self.DataSetName;
      Self.FrxDBDataSet.UserName:=Self.DataSetName;
    end;
    flag:=False;
    self.FrxDBDataSet.DataSet:=Self.ClientDataSet;
    Self.FrxReport.DataSets.Add(Self.FrxDBDataSet);
    
    Self.MasterData:= TFrxMasterData(Self.FrxReport.FindObject('MasterData_'+Self.DataSetName));
    if Assigned(Self.MasterData) then
    begin
     Self.MasterData.DataSet:=Self.FrxDBDataSet;
    end;
  end;
    FreeAndNil(Self.DesignStream);    //在 Destory 之前,提前释放，否则报表设计保存时保存不了（文件正在初当前进程占用）
end;

procedure TWebFastReport.OnGetValue(const VarName: String;  var Value: Variant);
var
  i:Integer;
  vv:RVariable;
begin
  for i := 0 to Self.ReportData.getVariableCount-1  do
  begin
    vv:=Self.ReportData.getRVariable(i);
    if VarName = vv.key then Value:= vv.value;
  end;
end;

function TWebFastReport.OnSaveReport(Report: TfrxReport;SaveAs: Boolean): Boolean;
var
  i:Integer;
  MasterDataName:string;
  Component:TfrxComponent;
  Path:string;
begin
  //Component:=nil;
  if true then
  begin
     //清除变量
     for i := 0 to Report.Variables.Count-1 do
     begin
        Report.Variables.DeleteVariable(Report.Variables.items[0].Name);    //使用items[0],因为删除时，变量个数在变化
     end;
     //清除数据集
     for  i:=0  to Report.DataSets.Count-1 do
     begin
        MasterDataName:='MasterData_'+Report.DataSets.Items[i].DataSetName;
        Component:=Report.FindObject(masterDataName);
        if Assigned(Component) and (Component is TfrxMasterData) then
        begin
            TfrxMasterData(Component).DataSet:=nil;
        end;
     end;
     Report.DataSets.Clear;
     Path:=ReportDataLoader.GetClientSaveDirectory(Self.AppName);
     if not DirectoryExists(Path) then
        ForceDirectories(Path);
     Report.SaveToFile(Path+'\'+self.ReportName+'.fr3');
  end;
  result:=true;
end;
destructor TWebFastReport.destroy;
begin
   FreeAndNil(Self.DataStream);
   FreeAndNil(self.DesignStream);
   FreeAndNil(Self.FrxDBDataSet);
   FreeAndNil(self.ClientDataSet);
   FreeAndNil(self.ReportData);
   FreeAndNil(Self.ReportDataLoader);
   FreeAndNil(Self.FrxDesgn);
   FreeAndNil(Self.FrxReport);
end;

procedure TWebFastReport.createReport;
begin
  Self.init(True);
  Self.FrxReport.DesignReport;
  FreeAndNil(Self.FrxReport);
end;

end.
