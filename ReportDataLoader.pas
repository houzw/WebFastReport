unit ReportDataLoader;

interface
uses
  SysUtils,Dialogs,Classes,ICS_HttpProt,NativeXml,SysComm,PerlRegEx;

type
  TReportDataLoader = class
     private
       Owner:TComponent;
       URL:WideString;
       ReportName:WideString;
       AppName:WideString;
       OnlyData:Boolean;


       HttpClient:THttpCli;
       HttpSendStream:TStream;
       HttpReceStream:TStream;

       ReponseText:string;
       DesignText:string;
       DataText:string;
       
       procedure OnRequestDone(Sender : TObject;RqType : THttpRequest;ErrCode  : Word);
       procedure Load();

     public           
       constructor create(Aowner:TComponent;AappName:WideString;AreportName: WideString;AUrl:WideString);
       destructor destroy(); override;
       function GetDesignStream():TStream;
       function GetDesignText():string;
       function GetDataStream():TStream;
       function GetDataText():string;
       function GetResponseText():string;
       class function GetClientSaveDirectory(AappName:WideString):WideString;
       class function GetClientSavePath(AappName:WideString;AreportName:WideString):WideString;
      
  end;

implementation

{ TReportDataLoader }

constructor TReportDataLoader.create(Aowner:TComponent;AappName:WideString;AreportName: WideString;AUrl:WideString);
var
  postData:string;
begin

  self.Owner:=Aowner;
  Self.AppName:=AappName;
  Self.ReportName:= AreportName;
  Self.URL:=AUrl;
  if FileExists(Self.GetClientSavePath(self.AppName,Self.ReportName)) then
  begin
    Self.OnlyData:=True;
  end
  else
  begin
    Self.OnlyData:=False;
  end;
  
  HttpSendStream:= TMemoryStream.Create;
  postData:='reportName='+UrlEncode(self.ReportName)+'&onlyData=';
  if self.OnlyData then
  begin
    postData:=postData+UrlEncode('true');
  end
  else
  begin
    postData:=postData+UrlEncode('false');
  end;
  HttpSendStream.Write(postData[1],Length(postData));
  HttpReceStream:= TMemoryStream.Create;
  Self.ReponseText:='100000';
  Self.Load;
end;

destructor TReportDataLoader.destroy;
begin
  Self.HttpSendStream.Free;
  Self.HttpReceStream.Free;
  HttpClient.Free;
end;

function TReportDataLoader.GetDataText: string;
begin
  Result:=self.DataText;
end;

function TReportDataLoader.GetDataStream: TStream;
var
  stream:TStream;
begin
   stream:=TStringStream.Create(Self.DataText);
   Result:=stream;
end;

function TReportDataLoader.GetDesignText: string;
begin
  Result:=self.DesignText;
end;

function TReportDataLoader.GetDesignStream: TStream;
var
  stream:TStream;
begin
  if Self.OnlyData then
  begin
     stream:=TFileStream.Create(Self.GetClientSavePath(self.AppName,self.ReportName),fmOpenRead or fmShareExclusive);
     Result:=stream;
  end
  else
  begin
     stream:=TStringStream.Create(UTF8Encode(Self.DesignText));
     Result:=stream;
  end;
end;

function TReportDataLoader.GetResponseText: string;
begin
  Result:=self.ReponseText;
end;

procedure TReportDataLoader.Load;
begin
  try
    HttpClient:=THttpCli.Create(nil);
    HttpClient.OnRequestDone:=self.OnRequestDone;
    HttpClient.URL:=Self.URL;
    HttpClient.SendStream:=Self.HttpSendStream;
    HttpClient.SendStream.Seek(0,0);
    HttpClient.RcvdStream:=Self.HttpReceStream;
    HttpClient.Post;
  except
//    on e:Exception do
//    begin
//      ShowMessage(e.message);
//    end;
  end;
end;

procedure TReportDataLoader.OnRequestDone(Sender: TObject; RqType: THttpRequest; ErrCode: Word);
var
  reg:TPerlRegEx;
  list:TStrings;
begin
  try
  if ErrCode <> 0 then
  begin
     ShowMessage('连接报表服务器异常，请检查网络或重试...');
  end;
  if HttpClient.StatusCode <> 200 then
  begin
    ShowMessage('服务器异常，请联系管理员...');
  end;
  HttpClient.RcvdStream.Seek(0,0);
  SetLength(ReponseText,HttpClient.RcvdStream.size);
  HttpClient.RcvdStream.Read(ReponseText[1], Length(ReponseText));

  //处理数据
  if self.OnlyData then
  begin
    Self.DesignText:='';
    self.DataText:=TSysCommon.DecodeZipBase64(Self.ReponseText);
  end
  else
  begin
    list:=TStringList.Create;
    reg:=TPerlRegEx.Create;
    reg.Subject:=Self.ReponseText;
    reg.RegEx:='\{1234567890\}';
    reg.Split(list,2);
    Self.DesignText:=TSysCommon.DecodeZipBase64(list.Strings[0]);
    Self.DataText:=TSysCommon.DecodeZipBase64(list.Strings[1]);
    FreeAndNil(reg);
    list.Free;
  end;
  HttpClient.OnRequestDone := nil;
  HttpClient.SendStream:=nil;
  HttpClient.RcvdStream:=nil;
  Self.HttpSendStream.Free;
  Self.HttpReceStream.Free;
  HttpClient.Free;
  except
//    on e:Exception do
//    begin
//      ShowMessage(e.message);
//    end;
  end;
end;

class function TReportDataLoader.GetClientSaveDirectory(AappName: WideString): WideString;
var
  d:WideString;
begin
  d:=GetEnvironmentVariable('USERPROFILE')+'\WebFastReport\'+AappName;
  Result:=d;
end;

class function TReportDataLoader.GetClientSavePath(AappName,AreportName: WideString): WideString;
var
  p:WideString;
begin
  p:=GetEnvironmentVariable('USERPROFILE')+'\WebFastReport\'+AappName+'\'+AreportName+'.fr3';
  Result:=p;
end;
end.
